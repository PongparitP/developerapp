import { Component, ViewChild } from '@angular/core';
import {
  Nav, Platform, ModalController, AlertController, LoadingController,
  IonicApp, App
} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PopoverController } from 'ionic-angular';

import { HomePage } from '../pages/home/home';
import { AdminPage } from '../pages/admin/admin';
import { UserPage } from '../pages/user/user';
import { UserPopoverPage } from '../pages/user-popover/user-popover';
import {ModalPage} from '../pages/modal/modal';
import {Device} from "@ionic-native/device";
import {Md5} from 'ts-md5/dist/md5';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { MenuController } from 'ionic-angular';

import { Users } from '../providers/users/users';
import { Language } from "../providers/language/lang";
import { LangData } from "../providers/language/lang.data";
import { ToastController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { AppMinimize } from '@ionic-native/app-minimize';
import { ENV } from '../config/mqtt.environment';


/**
 * Declare Paho mqtt that import in index.html because it's external js file
 */
declare let Paho: any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  /**
   *
   * MQTT VARIABLES
   */
  apiRequest = ENV().API_REQUEST;
  apiResponse = ENV().API_RESPONSE;
  loginUserApiUrl = ENV().API_LOGIN_USER;
  loginAdminApiUrl = ENV().API_LOGIN_ADMIN;
  allUserDeveloper = ENV().API_ALL_USER_DEV;
  updateUserDeveloperApiUrl = ENV().API_ALL_USER_DEV;
  client: any;
  requestLoginDate: any;
  requestgetAllUserDeveloperMqttConnectDate: any;
  requestUpdateUserDeveloperDate: any;
  message: any;

  rootPage: any = HomePage;

  loading: any;

  userSession = false;
  adminSession = false;
  searchInput: string;
  user: any;
  residences: any;
  // users: any;

  isLandscape: boolean;
  userTitle: string;
  adminTitle: string;
  isConnect = false;
  isBack = false;

  backButtonPressedOnceToExit: boolean;

  stringData: any;
  titleResidence = 'Panasonic';

  private app;
  private platform;
  private menu: MenuController;

  timeout: boolean;
  isAlert = false;
  isAddUser: boolean;


  pages: Array<{title: string, updateAt: string, component: any}>;

  constructor(platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public popoverCtrl: PopoverController,
              public modalCtrl: ModalController, public device: Device, private alertCtrl: AlertController, app: App,
              private screenOrientation: ScreenOrientation, public loadingCtrl: LoadingController, public users: Users,
              public menuCtrl: MenuController,public keyboard: Keyboard, menu: MenuController,
              public lang: Language, public langData: LangData, public toastCtrl: ToastController, public ionicApp: IonicApp,
              private appMinimize: AppMinimize) {
    localStorage.setItem('platformReady', 'false');
    this.menu = menu;
    this.app = app;
    this.platform = platform;
    this.initializeApp();
  }


  initializeApp() {
    this.langData.init();
    this.platform.ready().then(() => {
      localStorage.setItem('platformReady', 'true');
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      this.stringData = this.langData.getLanguageApp();
      if (this.stringData == null){
        this.stringData = {
          updateAt: '',
          userTitleLand: '',
          adminTitleLand: '',
          userTitlePort: '',
          adminTitlePort: '',
          pleaseWait: '',
          confirmOK: '',
          connectLost: ''
        };
      }
      console.log(JSON.stringify(this.stringData));

      this.user = JSON.parse(localStorage.getItem('user-session'));
      if (this.user == null) {
        this.Login();
        // this.userConnect();
      }
      else {
        if (this.user.session === 'user') {
          this.residences = JSON.parse(localStorage.getItem('getResidencesFor' + this.user.id));
          this.userConnect();
        }
        else if (this.user.session === 'admin') {
          this.users.setUsers(JSON.parse(localStorage.getItem('getUsersFor' + this.user.id)));
          this.adminConnect();
        }
      }

      this.statusBar.styleDefault();

      this.platform.registerBackButtonAction(() => {
        var ready = true;
        var activePortal = this.ionicApp._modalPortal.getActive() || this.ionicApp._modalPortal.getActive() ;
        var cangoback = this.nav.canGoBack();
        if (this.menuCtrl.isOpen()) {
          this.menuCtrl.close();
        }
        else {
          if (activePortal || cangoback) {
            ready = false;
            if (cangoback) {
              this.nav.pop().catch((error) => {
                console.log(error);
              });;
              ready = true;
            }
            else {
              var user = localStorage.getItem('user-session');
              if (user == null) {
                // this.toastExitApp();
                this.appMinimize.minimize();
              }
              else {
                activePortal.dismiss ();
                activePortal.onDidDismiss (() => {
                  ready = true;
                });
              }
            }
          }
          else {
            if (!cangoback) {
              // this.toastExitApp();
              this.appMinimize.minimize();
            }
            this.nav.pop().catch((error) => {
              console.log(error);
            });
          }
        }
      });
      if( this.splashScreen) {
        this.splashScreen.hide();
      }



      /**
       * set language
       *
       */
      // this.globalization.getPreferredLanguage()
      //   .then(res => {
      //     this.lang.setLanguage(res.value);
      //     this.langData.init();
      //     this.stringData = this.langData.getLanguageApp();
      //   })
      //   .catch(e => console.log(e));
      // this.langData.init();


      /**
       * check session from localstorage
       */

    });

    this.keyboard.disableScroll(false);
    this.keyboard.hideKeyboardAccessoryBar(false);

  }

  toastExitApp() {
    if (!this.isBack) {
      this.isBack = true;
      let toast = this.toastCtrl.create({
        message: this.stringData.toastMes,
        duration: 2000,
        position: 'bottom'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
        this.isBack = false;
      });
      toast.present();
    }
    else {
      this.platform.exitApp();
    }
  }

  /**
   * when open each page
   * @param page
   */
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (this.adminSession) {
      this.initializeMenu(this.user.session);
      localStorage.setItem('getUsersFor' + this.user.id, JSON.stringify(this.users.getUsers()));
      this.nav.setRoot (page.component, {adminData: page.userData, userData: page.resData});
    }
    if (this.userSession) {
      this.nav.setRoot (page.component, {userData: page.userData, residenceData: page.resData});
    }
  }

  /**
   * when click add user for admin level
   */
  addUser() {
    let profileModal = this.modalCtrl.create(ModalPage, { type: 'addUser' });
    profileModal.onDidDismiss(data => {
      if( data!= null) {
        if (data.type != 'dismiss') {
          this.presentLoadingDefalut();
          this.loading.present();
          this.isAddUser = true;
          this.updateUserDeveloperMqttConnect(data);
          if (!this.isAlert) {
            this.timeout = true;
            this.isAlert = true;
            var interval = setInterval(()=>{
              if (this.timeout) {
                clearInterval(interval);
                this.isAlert = false;
                this.loading.dismiss();
                let alert = this.alertCtrl.create({
                  title: this.stringData.connectLost,
                  buttons: [{
                    text: this.stringData.confirmOK,
                    handler: () => {

                    }
                  }]
                });
                alert.present();
              }
              else {
                this.isAlert = false;
                clearInterval(interval);
              }

            },60000);
          }
        }
      }

    });
    profileModal.present();
  }

  /**
   * popover on menu logout
   *
   */
  presentUserPopOver(myEvent) {
    let popover = this.popoverCtrl.create(UserPopoverPage, {data: this.user }, { cssClass: 'contact-popover'});
    popover.present({
      ev: myEvent
    });
  }

  /**
   *
   * login state when no session
   */
  Login() {
    /**
     * user mockup for test
     */
    // localStorage.removeItem('user-session');

    this.userSession = false;
    this.adminSession = false;
    // document.getElementById("residenceNavbar").innerText = "Panasonic";
    this.titleResidence = 'Panasonic';
    // document.getElementById("userNavbar").innerText = "";
    // document.getElementById("userNavbar").style.display = "none";

    this.pages = [];
    this.nav.setRoot(HomePage);
    this.createModal('login');
  }

  /**
   * when login and user is admin level
   */
  adminConnect() {
    if(this.loading != null) {
      this.loading.dismiss();
    }

    console.log("Admin State.");
    /**
     * user mockup for test
     */
    localStorage.setItem('user-session', JSON.stringify(this.user));

    this.userSession = false;
    this.adminSession = true;

    this.orientationCheck(this.screenOrientation.type);
    this.screenOrientation.onChange().subscribe(
      () => {
        this.orientationCheck(this.screenOrientation.type);
      }
    );

    // document.getElementById("residenceNavbar").innerText = this.shortDeveloper(this.user.developerName);
    this.titleResidence = this.shortDeveloper(this.user.developerName);
    // document.getElementById("residenceNavbarDiv").setAttribute('on-click', 'openMenu()');
    // document.getElementById("userNavbar").style.display = "block";
    // document.getElementById("userNavbar").innerText = this.user.firstname + ' ' + this.user.lastname;


    this.initializeMenu(this.user.session);
    this.nav.setRoot(HomePage);
  }

  /**
   * when login and user is user level
   */
  userConnect() {
    if(this.loading != null) {
      this.loading.dismiss();
    }
    localStorage.setItem('user-session', JSON.stringify(this.user));

    this.userSession = true;
    this.adminSession = false;

    this.orientationCheck(this.screenOrientation.type);
    this.screenOrientation.onChange().subscribe(
      () => {
        this.orientationCheck(this.screenOrientation.type);
      }
    );

    // document.getElementById("residenceNavbar").innerText = this.shortDeveloper(this.user.developer);
    this.titleResidence = this.shortDeveloper(this.user.developer);
    // document.getElementById("residenceNavbarDiv").setAttribute('ng-click', 'openMenu()');
    // document.getElementById("userNavbar").style.display = "block";
    // document.getElementById("userNavbar").innerText = this.user.firstname + ' ' + this.user.lastname;

    this.initializeMenu(this.user.session);
    this.nav.setRoot(HomePage);
  }

  /**
   * convert date as milliseconds to date string
   * @param date : date parameter
   * @param type : type of string that function would return between with time or without time
   * @returns {string} : date String
   */
  millisecondsToDate(date, type) {
    let year = date.getFullYear();
    let month = date.getMonth() +1;
    let day = date.getDate();
    let time = date.getTime();
    let hours = date.getHours();
    let minutes = parseInt(((time / (1000 * 60 )) % 60) + '');
    let hour = (hours < 10) ? "0" + hours : hours;
    let minute = (minutes < 10) ? "0" + minutes : minutes;

    if (type == 'withTime') {
      return day + '/' + month + '/' + year + ' ' + hour + ':' + minute;
    }
    else if (type == 'withoutTime') {
      return day + '/' + month + '/' + year;
    }
  }

  /**
   *
   * search algorhythm
   */
  onInput(input) {
    // Reset items back to all of the items
    this.initializeMenu(this.user.session);

    // set q to the value of the searchbar
    let q = input.srcElement.value;


    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }

    this.pages = this.pages.filter((v) => {
      if(v.title && q) {
        if (v.title.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  initializeMenu(session) {
    var pageSetup = [];
    if (session == 'admin') {
      for (var user of this.users.getUsers()) {

        var updatedAt = '';
        if (user.updatedAt != null) {
          var convertToDate = new Date(parseInt(user.updatedAt));
          updatedAt = this.millisecondsToDate(convertToDate, 'withoutTime');
        }

        pageSetup.push({
          title: user.firstName + ' ' + user.lastName,
          updatedAt: updatedAt,
          userData: this.user,
          resData: user,
          component: AdminPage
        })
      }
      this.pages = pageSetup;
    }
    else if (session == 'user') {
      console.log(this.user.permission);

      for (var residence of this.user.permission) {
        pageSetup.push({
          title: residence.name,
          updatedAt: null,
          resData: this.containResidence(residence.name, this.residences),
          userData: this.user,
          component: UserPage
        })
      }
      this.pages = pageSetup;
    }
  }

  createAlert(type, mes) {
    if (type === 'login') {
      let alert = this.alertCtrl.create({
        title: this.stringData.errLogin,
        subTitle: mes,
        buttons: [{
          text: this.stringData.confirmOK,
          handler: ()=> {
            this.createModal(type);
          }
        }]
      });
      alert.present();
    }
  }

  createModal (type) {
    let profileModal = this.modalCtrl.create(ModalPage, { type: type}, {showBackdrop: false});
    profileModal.onDidDismiss(data => {
      if (type === 'login') {
        this.presentLoadingDefalut();
        this.loading.present();
        if (data == null) {
          this.loading.dismiss();
          this.createModal('login');
        }
        else {
          if (!this.isAlert) {
            this.timeout = true;
            this.isAlert = true;
            var interval = setInterval(()=>{
              if (this.timeout) {
                clearInterval(interval);
                this.isAlert = false;
                this.loading.dismiss();
                let alert = this.alertCtrl.create({
                  title: this.stringData.connectLost,
                  buttons: [{
                    text: this.stringData.confirmOK,
                    handler: () => {

                    }
                  }]
                });
                alert.present();
                alert.onDidDismiss( () => {
                  this.createModal('login');
                });
              }
              else {
                this.isAlert = false;
                clearInterval(interval);
              }

            },60000);
          }
          this.loginUserMqttConnect(data);
        }

      }
    });
    profileModal.present();
  }

  containResidence(n, list) {
    for (var res of list ) {
      if (res.name === n) {
        return res;
      }
    }
    return false;
  }

  orientationCheck(type) {
    if (type === 'landscape' || type === 'lanscape-primary' ||
      type === 'landscape-secondary') {
      this.isLandscape = true;
      this.userTitle = this.stringData.userTitleLand;
      this.adminTitle =  this.stringData.adminTitleLand;
    }
    else if (type === 'portrait' || type === 'portrait-primary' ||
      type === 'portrait-secondary') {
      this.isLandscape = false;
      this.userTitle = this.stringData.userTitlePort;
      this.adminTitle =  this.stringData.adminTitlePort;
    }
  }

  shortDeveloper(devName) {
    if (devName.length >= 20) {
      return devName.substring(0,19) + '...';
    }
    else {
      return devName;
    }
  }

  presentLoadingDefalut() {
    this.loading = this.loadingCtrl.create({
      content: this.stringData.pleaseWait
    });
  }

  openMenu() {
    this.menuCtrl.toggle();
  }

  doRefresh(refresher) {
    this.initializeMenu(this.user.session);
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }


  /**
   * This function below is about mqtt connection
   */
  loginUserMqttConnect(data) {
    console.log('user');
    console.log(ENV().API_HOST, ENV().API_PORT, ENV().API_WS, ENV().API_USERNAME,
      ENV().API_PASSWORD,
      ENV().API_SSL,
      ENV().API_TIMEOUT);
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      this.timeout = false;
      var serveres = JSON.parse(mesdata.payloadString);
      if (serveres.header.requestId == this.requestLoginDate.toString()) {
        if (serveres.value.hasOwnProperty('error')) {
          /**
           * if user level cannot login then try to login via admin level
           */
          this.loginAdminMqttConnect(data);
        }
        else {
          /**
           * Login success
           */
          console.log(serveres);
          this.client.unsubscribe(this.loginUserApiUrl + this.apiResponse);
          var p = [];
          for (var rp of serveres.value.userInfo['residence_permission']) {
            p.push({
              id: rp['_id'],
              name: rp['residence_name']
            });
          }
          this.user = {
            id: serveres.value.userInfo['_id'],
            firstname: serveres.value.userInfo.firstName,
            lastname: serveres.value.userInfo.lastName,
            username: serveres.value.userInfo.username,
            email: serveres.value.userInfo.email,
            phone: serveres.value.userInfo.phone,
            developer: serveres.value.developerName,
            permission: p,
            session: 'user'
          }
          this.residences = [];
          for (var res of serveres.value.residence) {
            this.residences.push({
              id: res['_id'],
              updatedAt: res.updateAt,
              createdAt: res.createdAt,
              name: res.name,
              houses: res.houses,
              messages: res.notifications
            });
          }
          localStorage.setItem('getResidencesFor' + this.user.id, JSON.stringify(this.residences));
          this.userConnect();
        }
        // this.client.unsubscribe(this.loginUserApiUrl + this.apiResponse);
      }
    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        console.log('on connect');
        this.requestLoginDate = new Date().getTime();
        this.requestLoginDate = parseInt(this.requestLoginDate);
        this.client.subscribe(this.loginUserApiUrl + this.apiResponse, {qos: 2});
        var m = JSON.stringify({
          header: {
            requestId: this.requestLoginDate + "",
            command: 'data',
            timestamp: this.requestLoginDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: {
            username: data.username,
            password: Md5.hashStr(data.password)
          }
        });
        console.log(m);
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.loginUserApiUrl + this.apiRequest;
        this.client.send(this.message);
      },
      onFailure: () => {
        this.timeout = false;
        if ( this.loading != null) {
          this.loading.dismiss();
        }
        let alert = this.alertCtrl.create({
          title: this.stringData.connectLost,
          buttons: [{
            text: this.stringData.confirmOK,
            handler: () => {
              this.createModal('login');
            }
          }]
        });
        alert.present();
      }
    });
  }

  loginAdminMqttConnect(data) {
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      var serveres = JSON.parse(mesdata.payloadString);
      console.log('in');
      if (serveres.header.requestId == this.requestLoginDate.toString()) {
        if (serveres.value.hasOwnProperty('error')) {
          /**
           * login failed
           */
          if ( this.loading != null) {
            this.loading.dismiss();
          }
          this.createAlert('login', serveres.value.error);
        }
        else {
          /**
           * Login success
           */
          // console.log('User Value : ' + JSON.stringify(serveres.value));
          this.user = {
            developerId: serveres.value[0]['_id'],
            developerName: serveres.value[0].name,
            firstname: 'Admin',
            lastname: '',
            session: 'admin'
          }
          this.getAllUserDeveloperMqttConnect('init');
        }
        // this.client.unsubscribe(this.loginAdminApiUrl + this.apiResponse);
      }

    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        this.requestLoginDate = new Date().getTime();
        this.requestLoginDate = parseInt(this.requestLoginDate);
        this.client.subscribe(this.loginAdminApiUrl + this.apiResponse, {qos: 2});
        var m = JSON.stringify({
          header: {
            requestId: this.requestLoginDate + "",
            command: 'data',
            timestamp: this.requestLoginDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: {
            username: data.username,
            password: Md5.hashStr(data.password)
            // password: data.password
          }
        });
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.loginAdminApiUrl + this.apiRequest;
        this.client.send(this.message);
      },
      onFailure: () => {
        this.timeout = false;
        if ( this.loading != null) {
          this.loading.dismiss();
        }
        let alert = this.alertCtrl.create({
          title: this.stringData.connectLost,
          buttons: [{
            text: this.stringData.confirmOK,
            handler: () => {
              this.createModal('login');
            }
          }]
        });
        alert.present();
      }
    });
  }

  getAllUserDeveloperMqttConnect(type) {
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      var serveres = JSON.parse(mesdata.payloadString);
      console.log('data : ' + JSON.stringify(serveres));
      if (serveres.header.requestId == this.requestgetAllUserDeveloperMqttConnectDate.toString()) {
        if (serveres.value.hasOwnProperty('error')) {
          /**
           * failed to get all user developer
           */
          if ( this.loading != null) {
            this.loading.dismiss();
          }
          alert(serveres.value.error);
        }
        else {
          /**
           * get all user developer
           */
          this.users.setUsers(serveres.value.users);
          localStorage.setItem('getUsersFor' + this.user.id, JSON.stringify(this.users.getUsers()));
          if (type === 'createUser') {
            this.loading.dismiss();
            if (this.isAddUser) {
              let alert = this.alertCtrl.create({
                title: this.stringData.confirmAddUser,
                buttons: [{
                  text: this.stringData.confirmOK,
                  handler: () => {
                    this.isAddUser = false;
                  }
                }]
              });
              alert.present();
            }

            this.initializeMenu(this.user.session);
          }
          else if (type === 'init') {
            this.adminConnect();
          }
        }
        // this.client.unsubscribe(this.allUserDeveloper + this.user.daveloperId + this.apiResponse);
      }

    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        console.log('in');
        this.requestgetAllUserDeveloperMqttConnectDate = new Date().getTime();
        this.requestgetAllUserDeveloperMqttConnectDate = parseInt(this.requestgetAllUserDeveloperMqttConnectDate);
        this.client.subscribe(this.allUserDeveloper + this.user.daveloperId + this.apiResponse, {qos: 2});
        var m = JSON.stringify({
          header: {
            requestId: this.requestgetAllUserDeveloperMqttConnectDate + "",
            command: 'get',
            timestamp: this.requestgetAllUserDeveloperMqttConnectDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: {
            developerName: this.user.developerName,
          }
        });
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.allUserDeveloper + this.user.daveloperId + this.apiRequest;
        this.client.send(this.message);
      }
    });
  }

  updateUserDeveloperMqttConnect(data) {
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      this.timeout = false;
      var serveres = JSON.parse(mesdata.payloadString);
      if (serveres.header.requestId == this.requestUpdateUserDeveloperDate.toString()) {
        if (serveres.value.hasOwnProperty ('error')) {
          /**
           * failed to create/update user developer
           */
          this.loading.dismiss ();
          alert (serveres.value.error);
          this.addUser();
        }
        else {
          /**
           * success to create/update user developer
           */
          this.getAllUserDeveloperMqttConnect('createUser');
          // console.log('User Value : ' + JSON.stringify(serveres.value).length);

        }
        // this.client.unsubscribe(this.updateUserDeveloperApiUrl + this.user.developerId + this.apiResponse);
      }

    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        console.log(JSON.stringify(data));
        this.requestUpdateUserDeveloperDate = new Date().getTime();
        this.requestUpdateUserDeveloperDate = parseInt(this.requestUpdateUserDeveloperDate);
        this.client.subscribe(this.updateUserDeveloperApiUrl + this.user.developerId + this.apiResponse, {qos: 2});
        var cm = 'create';
        var m = JSON.stringify({
          header: {
            requestId: this.requestUpdateUserDeveloperDate + "",
            command: cm,
            timestamp: this.requestUpdateUserDeveloperDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: {
            developerName: this.user.developerName,
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            phone: data.phone,
            username: data.username,
            password: Md5.hashStr(data.password),
            residence_permission: []
          }
        });
        // console.log(this.admin.developerName + ' : developer Name');
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.updateUserDeveloperApiUrl + this.user.developerId + this.apiRequest;
        this.client.send(this.message);
      },
      onFailure: () => {
        this.timeout = false;
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          title: this.stringData.connectLost,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
    });
  }
}
