import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AdminPage } from '../pages/admin/admin';
import { UserPage } from '../pages/user/user';
import { UserPopoverPage } from '../pages/user-popover/user-popover';
import { ModalPage } from '../pages/modal/modal';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DatePicker } from '@ionic-native/date-picker';
import { Device } from '@ionic-native/device';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Users } from '../providers/users/users';
import { Language } from "../providers/language/lang";
import { LangData } from "../providers/language/lang.data";
import { Globalization } from "@ionic-native/globalization";
import { Keyboard } from "@ionic-native/keyboard";

import { AppMinimize } from '@ionic-native/app-minimize';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AdminPage,
    UserPage,
    UserPopoverPage,
    ModalPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AdminPage,
    UserPage,
    UserPopoverPage,
    ModalPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePicker,
    Device,
    ScreenOrientation,
    Users,
    Globalization,
    Language,
    LangData,
    Keyboard,
    AppMinimize,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
