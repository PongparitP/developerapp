export var ENV = () => {
    return {
      API_HOST                  : 'n1mq2.pst.solutions',
      API_PORT                  : Number(8884),
      API_SSL                   : true,
      API_TIMEOUT               : 15,
      API_WS                    : '/ws',
      API_USERNAME              : 'admin',
      API_PASSWORD              : 'admin.kisra2',
      API_REQUEST               : '.REQUEST',
      API_RESPONSE              : '.RESPONSE',
      API_LOGIN_USER            : '.APP_PST_SOLUTIONS.RESIDENCE.V0_1_0.DEVELOPER.LOGIN.NORMAL',
      API_LOGIN_ADMIN           : '.APP_PST_SOLUTIONS.RESIDENCE.V0_1_0.DEVELOPER.LOGIN.ADMIN',
      API_ALL_USER_DEV          : '.APP_PST_SOLUTIONS.RESIDENCE.V0_1_0.DEVELOPER.USER.',
      API_RES_DEV               : '.APP_PST_SOLUTIONS.RESIDENCE.V0_1_0.RESIDENCE.DEV.',
      API_HOUSE_DEV             : '.APP_PST_SOLUTIONS.RESIDENCE.V0_1_0.HOUSE.DEV.',
      API_NOTIFICATION          : '.APP_PST_SOLUTIONS.RESIDENCE.V0_1_0.NOTIFICATION.'
    }
  }
