import { Component } from '@angular/core';
import {NavController, NavParams, ModalController, AlertController, LoadingController, Platform} from 'ionic-angular';
import {ModalPage} from '../modal/modal';
import {Device} from "@ionic-native/device";
import {Md5} from 'ts-md5/dist/md5';
import { Users } from '../../providers/users/users'
import { LangData } from "../../providers/language/lang.data";
import {Language} from "../../providers/language/lang";
import { ENV } from "../../config/mqtt.environment";

declare let Paho: any;



@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})
export class AdminPage {

  /**
   * mqtt connection variables
   */
  client: any;
  message: any;
  requestGetAllResidenceDate: any;
  requestUpdateUserDeveloperDate: any;
  requestgetAllUserDeveloperMqttConnectDate: any;
  apiResponse = ENV().API_RESPONSE;
  apiRequest = ENV().API_REQUEST;
  allResidenceApiUrl = ENV().API_RES_DEV;
  updateUserDeveloperApiUrl = ENV().API_ALL_USER_DEV;
  allUserDeveloper = ENV().API_ALL_USER_DEV;

  loading: any;

  user: any;
  admin: any;
  residences = [];

  isToggleChange = false;
  isRefresh = false;

  stringData: any;

  timeout: boolean;
  isAlert = false;
  isEditUser: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, private alertCtrl: AlertController,
              private device: Device, public loadingCtrl: LoadingController, public users: Users,
              public langData: LangData, public lang: Language, public platform: Platform) {
    this.user = this.navParams.get('userData');
    this.admin = this.navParams.get('adminData');

    // this.globalization.getPreferredLanguage()
    //   .then(res => {
    //     this.lang.setLanguage(res.value);
    //     this.langData.init();
    //     this.stringData = this.langData.getLanguageAdmin();
    //   })
    //   .catch(e => console.log(e));
    this.langData.init();
    this.stringData = this.langData.getLanguageAdmin();

    this.presentLoadingDefalut();
    this.loading.present();
    this.getAllResidenceMqttConnect();
    if (!this.isAlert) {
      this.timeout = true;
      this.isAlert = true;
      var interval = setInterval(()=>{
        if (this.timeout) {
          clearInterval(interval);
          this.isAlert = false;
          this.loading.dismiss();
          let alert = this.alertCtrl.create({
            title: this.stringData.connectLost,
            buttons: [{
              text: this.stringData.confirmOK,
              handler: () => {

              }
            }]
          });
          alert.present();
        }
        else {
          this.isAlert = false;
          clearInterval(interval);
        }

      },60000);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminPage');
    if (this.platform.is('ios')){

    }
  }

  openEditProfile() {
    let profileModal = this.modalCtrl.create(ModalPage, { type: 'editProfileUser',
                                                                data: this.user});
    profileModal.onDidDismiss(data => {
      if (data != null) {
        if (data.type != 'dismiss') {
          this.presentLoadingDefalut();
          this.loading.present();
          this.isEditUser = true;
          this.updateUserDeveloperMqttConnect('updateUser', data);
          if (!this.isAlert) {
            this.timeout = true;
            this.isAlert = true;
            var interval = setInterval(()=>{
              if (this.timeout) {
                clearInterval(interval);
                this.isAlert = false;
                this.loading.dismiss();
                let alert = this.alertCtrl.create({
                  title: this.stringData.connectLost,
                  buttons: [{
                    text: this.stringData.confirmOK,
                    handler: () => {

                    }
                  }]
                });
                alert.present();
              }
              else {
                this.isAlert = false;
                clearInterval(interval);
              }

            },60000);
          }
        }
      }

    });
    profileModal.present();
  }

  changePermission(toggle, index) {
    if (this.isToggleChange) {
      this.isToggleChange = false;
    }
    else {
      var checked = toggle.checked;
      let alert = this.alertCtrl.create({
        title: this.stringData.confirmChangeTitle,
        buttons: [
          {
            text: this.stringData.confirmCancel,
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
              this.isToggleChange = true;
              toggle.checked = !checked;
            }
          },
          {
            text: this.stringData.confirmOK,
            handler: () => {
              this.residences[index].enable = toggle.checked;
              this.presentLoadingDefalut();
              this.loading.present();
              this.updateUserDeveloperMqttConnect('updatePermission', {toggle: toggle, index: index, check: checked});
              if (!this.isAlert) {
                this.timeout = true;
                this.isAlert = true;
                var interval = setInterval(()=>{
                  if (this.timeout) {
                    clearInterval(interval);
                    this.isAlert = false;
                    this.loading.dismiss();
                    this.isToggleChange = true;
                    toggle.checked = !checked;
                    this.residences[index].enable = toggle.checked;
                    let alert = this.alertCtrl.create({
                      title: this.stringData.connectLost,
                      buttons: [{
                        text: this.stringData.confirmOK,
                        handler: () => {
                        }
                      }]
                    });
                    alert.present();
                  }
                  else {
                    this.isAlert = false;
                    clearInterval(interval);
                  }

                },60000);
              }
            }
          }
        ]
      });
      alert.present();
    }
  }

  presentLoadingDefalut() {
    this.loading = this.loadingCtrl.create({
      content: this.stringData.pleaseWait
    });
  }

  findUser(ul) {
    for (var user of ul) {
      if (this.user['_id'] === user['_id']) {
        this.user = user;
        return true;
      }
    }
    return false;
  }

  doRefresh(refresher) {
    this.isRefresh = true;
    this.getAllUserDeveloperMqttConnect();
    setTimeout(() => {
      refresher.complete();
      this.isRefresh = false;
    }, 2000);
  }


  /**
   * this function below is about mqtt connect
   */
  getAllResidenceMqttConnect() {
    this.timeout = false;
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      console.log('connect success');
      var serveres = JSON.parse(mesdata.payloadString);
      if (serveres.header.requestId == this.requestGetAllResidenceDate.toString()) {
        if (serveres.value.hasOwnProperty ('error')) {
          /**
           * failed to get all user developer
           */
          if (!this.isRefresh) {
            this.loading.dismiss();
          }
          alert (serveres.value.error);
        }
        else {
          /**
           * get all user developer
           */
          this.residences = [];
          for (var res of serveres.value) {
            var p = false;
            for (var pu of this.user['residence_permission']) {
              if (pu['residence_name'] == res.name) {
                p = true;
                break;
              }
            }
            var resPush = {
              id: res['_id'],
              name: res.name,
              permission: p
            };
            this.residences.push (resPush);
          }

          if (!this.isRefresh) {
            this.loading.dismiss();
            if (this.isEditUser) {
              let alert = this.alertCtrl.create({
                title: this.stringData.confirmEditUser,
                buttons: [{
                  text: this.stringData.confirmOK,
                  handler: () => {
                    this.isEditUser = false;
                  }
                }]
              });
              alert.present();
            }

          }
        // console.log('User Value : ' + JSON.stringify(serveres.value).length);

        }
        // this.client.unsubscribe(this.allResidenceApiUrl + this.admin.developerId + this.apiResponse);
      }

    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        console.log('connect to admin');
        this.requestGetAllResidenceDate = new Date().getTime();
        this.requestGetAllResidenceDate = parseInt(this.requestGetAllResidenceDate);
        this.client.subscribe(this.allResidenceApiUrl + this.admin.developerId + this.apiResponse, {qos: 2});
        var m = JSON.stringify({
          header: {
            requestId: this.requestGetAllResidenceDate + "",
            command: 'get',
            timestamp: this.requestGetAllResidenceDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: {
            developerName: this.admin.developerName,
          }
        });
        // console.log(this.admin.developerName + ' : developer Name');
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.allResidenceApiUrl + this.admin.developerId + this.apiRequest;
        this.client.send(this.message);
      },
      onFailure: () => {
        if(this.loading != null) {
          this.loading.dismiss();
          this.timeout = false;
          let alert = this.alertCtrl.create({
            title: this.stringData.connectLost,
            buttons: [{
              text: this.stringData.confirmOK,
              handler: () => {
              }
            }]
          });
          alert.present();
        }


      }
    });
  }

  updateUserDeveloperMqttConnect(type, data) {
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      this.timeout = false;
      var serveres = JSON.parse(mesdata.payloadString);
      if (serveres.header.requestId == this.requestUpdateUserDeveloperDate.toString()) {
        if (serveres.value.hasOwnProperty ('error')) {
          /**
           * failed to create/update user developer
           */
          this.loading.dismiss ();
          alert (serveres.value.error);
        }
        else {
          /**
           * success to create/update user developer
           */
          this.getAllUserDeveloperMqttConnect();
          // console.log('User Value : ' + JSON.stringify(serveres.value).length);

        }
        // this.client.unsubscribe(this.updateUserDeveloperApiUrl + this.admin.developerId + this.apiResponse);
      }

    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        this.requestUpdateUserDeveloperDate = new Date().getTime();
        this.requestUpdateUserDeveloperDate = parseInt(this.requestUpdateUserDeveloperDate);
        this.client.subscribe(this.updateUserDeveloperApiUrl + this.admin.developerId + this.apiResponse, {qos: 2});
        var v = {};
        var cm = '';
        switch (type) {
          case 'create':
            v = {
              firstName: data.firstName,
              lastName: data.lastName,
              email: data.email,
              phone: data.phone,
              username: data.username,
              password: Md5.hashStr(this.user.password),
              residence_permission: []
            };
            cm = 'create';
            break;
          case 'updatePermission':
            var pu = [];
            for (var res of this.residences) {
              if (res.permission) {
                var r = {
                  'residence_name': res.name,
                  '_id': res.id
                };
                pu.push(r);
              }
            }
            v = {
              id: this.user['_id'],
              firstName: this.user.firstName,
              lastName: this.user.lastName,
              email: this.user.email,
              phone: this.user.phone,
              username: this.user.username,
              password: this.user.password,
              residence_permission: pu
            };
            cm = 'update';
            break;
          case 'updateUser':
            v = {
              id: this.user['_id'],
              firstName: data.firstName,
              lastName: data.lastName,
              email: data.email,
              phone: data.phone,
              username: data.username,
              password: Md5.hashStr(data.password),
              residence_permission: this.user['residence_permission']
            };
            cm = 'update';
            break;

        }
        console.log('user : ' + JSON.stringify(this.user));
        var m = JSON.stringify({
          header: {
            requestId: this.requestUpdateUserDeveloperDate + "",
            command: cm,
            timestamp: this.requestUpdateUserDeveloperDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: v
        });
        // console.log(this.admin.developerName + ' : developer Name');
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.updateUserDeveloperApiUrl + this.admin.developerId + this.apiRequest;
        this.client.send(this.message);
      },
      onFailure: () => {
        this.timeout = false;
        this.loading.dismiss();
        if (type == 'updatePermission') {
          this.isToggleChange = true;
          data.toggle.checked = !data.toggle.checked;
          this.residences[data.index].enable = data.toggle.checked;
          console.log(data);
        }

        let alert = this.alertCtrl.create({
          title: this.stringData.connectLost,
          buttons: [{
            text: this.stringData.confirmOK,
            handler: () => {
            }
          }]
        });
        alert.present();
      }
    });
  }

  getAllUserDeveloperMqttConnect() {
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      var serveres = JSON.parse(mesdata.payloadString);
      console.log('data : ' + JSON.stringify(serveres));
      if (serveres.header.requestId == this.requestgetAllUserDeveloperMqttConnectDate.toString()) {
        if (serveres.value.hasOwnProperty('error')) {
          /**
           * failed to get all user developer
           */
          if (!this.isRefresh) {
            this.loading.dismiss();
          }
          alert(serveres.value.error);
        }
        else {
          /**
           * get all user developer
           */
          if (this.findUser(serveres.value.users)) {
            this.users.setUsers(serveres.value.users);
            this.getAllResidenceMqttConnect();
          }

        }
        // this.client.unsubscribe(this.allUserDeveloper + this.admin.daveloperId + this.apiResponse);
      }

    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        console.log('in');
        this.requestgetAllUserDeveloperMqttConnectDate = new Date().getTime();
        this.requestgetAllUserDeveloperMqttConnectDate = parseInt(this.requestgetAllUserDeveloperMqttConnectDate);
        this.client.subscribe(this.allUserDeveloper + this.admin.daveloperId + this.apiResponse, {qos: 2});
        var m = JSON.stringify({
          header: {
            requestId: this.requestgetAllUserDeveloperMqttConnectDate + "",
            command: 'get',
            timestamp: this.requestgetAllUserDeveloperMqttConnectDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: {
            developerName: this.admin.developerName,
          }
        });
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.allUserDeveloper + this.admin.daveloperId + this.apiRequest;
        this.client.send(this.message);
      }
    });
  }

}
