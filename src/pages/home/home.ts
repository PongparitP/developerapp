import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LangData } from "../../providers/language/lang.data";
import {Language} from "../../providers/language/lang";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  adminSession: boolean;
  userSession: boolean;

  stringData: any;

  adminText = '';
  userText = '';


  constructor(public navCtrl: NavController,
              public langData: LangData,
              public lang: Language) {
    // this.globalization.getPreferredLanguage()
    //   .then(res => {
    //     this.lang.setLanguage(res.value);
    //     this.langData.init();
    //     this.stringData = this.langData.getLanguageHome();
    //   })
    //   .catch(e => console.log(e));
    this.stringData = this.langData.getLanguageHome();
    if (this.stringData == null) {
      this.stringData = {
        adminSelect: '',
        userSelect: ''
      }
    }




  }

  ionViewDidLoad() {
    // console.log('in home page');
    var user = JSON.parse(localStorage.getItem('user-session'));
    if (user != null) {
      if (user.session == 'admin') {
        this.adminSession = true;
        this.userSession = false;
      }
      else if (user.session == 'user') {
        this.adminSession = false;
        this.userSession = true;
      }
    }
    else {
      this.adminSession = false;
      this.userSession = false;
    }
  }

}
