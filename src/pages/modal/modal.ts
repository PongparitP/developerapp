import { Component } from '@angular/core';
import {NavController, NavParams, ViewController, AlertController, Platform} from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { LangData } from "../../providers/language/lang.data";
import {Language} from "../../providers/language/lang";

@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  isAddUser = false;
  isEditProfileUser = false;
  isCreateMessage = false;
  isOpenNotice = false;
  isEditNotice = false;
  isLogin = false;
  isIos = false;

  user: any;
  message: any;

  firstname: string;
  lastname: string;
  password: string;
  username: string;
  cfmpassword: string;
  email: string;
  phone: string;

  title: string;
  description: string;
  date: string;
  dateData: Date;
  updateAt: string;

  stringData: any;




  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private alertCtrl: AlertController, private datePicker: DatePicker,
              public langData: LangData, public lang: Language, public platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
    // this.globalization.getPreferredLanguage()
    //   .then(res => {
    //     this.lang.setLanguage(res.value);
    //     this.langData.init();
    //     this.stringData = this.langData.getLanguageModal();
    //   })
    //   .catch(e => console.log(e));
    this.stringData = this.langData.getLanguageModal();
    this.checkIos();
    if (this.navParams.get('type') == 'login') {
      this.isAddUser = false;
      this.isEditProfileUser = false;
      this.isCreateMessage = false;
      this.isOpenNotice = false;
      this.isEditNotice = false;
      this.isLogin = true;
    }
    else if (this.navParams.get('type') == 'addUser') {
      this.isAddUser = true;
      this.isEditProfileUser = false;
      this.isCreateMessage = false;
      this.isOpenNotice = false;
      this.isEditNotice = false;
      this.isLogin = false;
    }
    else if (this.navParams.get('type') == 'editProfileUser') {
      this.isAddUser = false;
      this.isEditProfileUser = true;
      this.isCreateMessage = false;
      this.isOpenNotice = false;
      this.isEditNotice = false;
      this.isLogin = false;

      /**
       *  get data in textfield input
       */
      this.user = this.navParams.get('data');
      console.log(JSON.stringify(this.user));
      this.firstname = this.user.firstName;
      this.lastname = this.user.lastName;
      this.username = this.user.username;
      this.email = this.user.email;
      this.phone = this.user.phone.replace(/-/g, '');
    }
    else if (this.navParams.get('type') == 'createMessage') {
      this.isAddUser = false;
      this.isEditProfileUser = false;
      this.isCreateMessage = true;
      this.isOpenNotice = false;
      this.isEditNotice = false;
      this.isLogin = false;


      this.user = this.navParams.get('data');
    }
    else if (this.navParams.get('type') == 'openNotice') {
      this.isAddUser = false;
      this.isEditProfileUser = false;
      this.isCreateMessage = false;
      this.isOpenNotice = true;
      this.isEditNotice = false;
      this.isLogin = false;

      this.user = this.navParams.get('data');
      this.message = this.navParams.get('notice');

      this.title = this.message.topic;
      this.description = this.message.message;
      var d = new Date(parseInt(this.message.sendDate));
      this.date = this.millisecondsToDate(d, 'withTime');
      this.dateData = d;
      this.updateAt = this.message.createDate;
    }
    else if (this.navParams.get('type') == 'editNotice') {
      this.isAddUser = false;
      this.isEditProfileUser = false;
      this.isCreateMessage = false;
      this.isOpenNotice = false;
      this.isEditNotice = true;
      this.isLogin = false;

      this.user = this.navParams.get('data');
      this.message = this.navParams.get('notice');
      console.log(JSON.stringify(this.message));

      this.title = this.message.topic;
      this.description = this.message.message;
      var d2 = new Date(parseInt(this.message.sendDate));
      this.date = this.millisecondsToDate(d2, 'withTime');
      this.dateData = d2;
    }
  }

  dismiss() {
    let data = {
      type: 'dismiss'
    }
    this.viewCtrl.dismiss(data);
  }

  addUserProgress() {
    if (this.firstname != null && this.lastname != null && this.username != null && this.password != null && this.cfmpassword != null && this.email != null && this.phone != null) {
      if (this.password != this.cfmpassword) {
        let alert = this.alertCtrl.create({
          title: this.stringData.passwordErrorTitle,
          subTitle: this.stringData.passwordErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else {
        if (this.validateEmail(this.email)) {
          if (this.phone.indexOf('-') > -1) {
            this.phone.replace(/-/g, '');
          }
          this.phone = this.phone.substring(0, 3) + '-' + this.phone.substring(3, 6) + '-' + this.phone.substring(6, 10);
          var data = {
            firstName: this.firstname,
            lastName: this.lastname,
            username: this.username,
            password: this.password,
            email: this.email,
            phone: this.phone
          }
          this.viewCtrl.dismiss(data);
        }
        else {
          let alert = this.alertCtrl.create({
            title: this.stringData.emailErrorTitle,
            subTitle: this.stringData.emailErrorSubTitle,
            buttons: [this.stringData.confirmOK]
          });
          alert.present();
        }
      }

    }
    else {
      if (this.firstname == null || this.firstname == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.addUserFirstNameErrorTitle,
          subTitle: this.stringData.addUserFirstNameErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.lastname == null || this.lastname == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.addUserLastNameErrorTitle,
          subTitle: this.stringData.addUserLastNameErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.username == null || this.username == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.addUserUsernameErrorTitle,
          subTitle: this.stringData.addUserUsernameErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.password == null || this.password == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.addUserPasswordErrorTitle,
          subTitle: this.stringData.addUserPasswordErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.cfmpassword == null || this.cfmpassword == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.addUserCfmPasswordErrorTitle,
          subTitle: this.stringData.addUserCfmPasswordErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.email == null || this.email == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.addUserEmailErrorTitle,
          subTitle: this.stringData.addUserEmailErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.phone == null || this.phone == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.addUserPhoneErrorTitle,
          subTitle: this.stringData.addUserPhoneErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else {
        let alert = this.alertCtrl.create({
          title: this.stringData.addUserErrorTitle,
          subTitle: this.stringData.addUserErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }

    }
  }

  editProfileUserProgress() {
    if (this.firstname != null && this.lastname != null && this.username != null && this.password != null && this.cfmpassword != null && this.email != null && this.phone != null) {
      if (this.password != this.cfmpassword) {
        let alert = this.alertCtrl.create({
          title: this.stringData.passwordErrorTitle,
          subTitle: this.stringData.passwordErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else {
        if (this.validateEmail(this.email)) {
          if (this.phone.indexOf('-') > -1) {
            this.phone.replace(/-/g, '');
          }
          this.phone = this.phone.substring(0, 3) + '-' + this.phone.substring(3, 6) + '-' + this.phone.substring(6, 10);
          var data = {
            firstName: this.firstname,
            lastName: this.lastname,
            username: this.username,
            password: this.password,
            email: this.email,
            phone: this.phone
          }

          this.viewCtrl.dismiss(data);
        }
        else {
          let alert = this.alertCtrl.create({
            title: this.stringData.emailErrorTitle,
            subTitle: this.stringData.emailErrorSubTitle,
            buttons: [this.stringData.confirmOK]
          });
          alert.present();
        }
      }

    }
    else {
      if (this.firstname == null || this.firstname == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.editUserFirstNameErrorTitle,
          subTitle: this.stringData.editUserFirstNameErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.lastname == null || this.lastname == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.editUserLastNameErrorTitle,
          subTitle: this.stringData.editUserLastNameErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.username == null || this.username == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.editUserUsernameErrorTitle,
          subTitle: this.stringData.editUserUsernameErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.password == null || this.password == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.editUserPasswordErrorTitle,
          subTitle: this.stringData.editUserPasswordErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.cfmpassword == null || this.cfmpassword == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.editUserCfmPasswordErrorTitle,
          subTitle: this.stringData.editUserCfmPasswordErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.email == null || this.email == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.editUserEmailErrorTitle,
          subTitle: this.stringData.editUserEmailErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else if (this.phone == null || this.phone == '') {
        let alert = this.alertCtrl.create({
          title: this.stringData.editUserPhoneErrorTitle,
          subTitle: this.stringData.editUserPhoneErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
      else {
        let alert = this.alertCtrl.create({
          title: this.stringData.editUserErrorTitle,
          subTitle: this.stringData.editUserErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }

    }
  }

  messageProgress() {
    var data = {};
    if (this.navParams.get('type') == 'createMessage') {
      if (this.title != null && this.dateData != null && this.title != '' && this.description != null && this.description != '') {
        var now = new Date().getTime();
        if (this.dateData.getTime() <= now ) {
          let alert = this.alertCtrl.create({
            title: this.stringData.timeMesErrorTitle,
            subTitle: this.stringData.timeMesErrorSubTitle,
            buttons: [this.stringData.confirmOK]
          });
          alert.present();
        }
        else {
          data = {
            topic: this.title,
            message: this.description,
            sentDate: this.dateData
          }
          this.viewCtrl.dismiss(data);
        }

      } else {
        let alert = this.alertCtrl.create({
          title: this.stringData.createMesErrorTitle,
          subTitle: this.stringData.createMesErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }

    }
    else {
      if (this.title != null && this.dateData != null && this.title != '' && this.description != null && this.description != '') {
        var now = new Date().getTime();
        if (this.dateData.getTime() <= now ) {
          let alert = this.alertCtrl.create({
            title: this.stringData.timeMesErrorTitle,
            subTitle: this.stringData.timeMesErrorSubTitle,
            buttons: [this.stringData.confirmOK]
          });
          alert.present();
        }
        else {
          data = {
            id: this.message['_id'],
            topic: this.title,
            message: this.description,
            sentDate: this.dateData
          }
          this.viewCtrl.dismiss(data);
        }

      } else {
        let alert = this.alertCtrl.create({
          title: this.stringData.updateMesErrorTitle,
          subTitle: this.stringData.updateMesErrorSubTitle,
          buttons: [this.stringData.confirmOK]
        });
        alert.present();
      }
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(email);
  }

  showDatePicker () {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
          this.date = this.millisecondsToDate(date, 'withTime'),
          this.dateData = date
      },
    err => console.log(err)
    );
  }

  millisecondsToDate(date, type) {
    var year = date.getFullYear();
    var month = date.getMonth() +1;
    var day = date.getDate();
    var time = date.getTime();
    var hours = parseInt(date.getHours());
    var minutes = parseInt(((time / (1000 * 60 )) % 60) + '');
    var hour = (hours < 10) ? "0" + hours : hours;
    var minute = (minutes < 10) ? "0" + minutes : minutes;

    if (type == 'withTime') {
      return day + '/' + month + '/' + year + ' ' + hour + ':' + minute;
    }
    else if (type == 'withoutTime') {
      return day + '/' + month + '/' + year;
    }
  }

  loginProgress() {
    if (this.username != null && this.username != '' && this.password != null && this.password != '') {
      var data = {
        username: this.username,
        password: this.password
      }
      this.viewCtrl.dismiss(data);
    }
    else {
      let alert = this.alertCtrl.create({
        title: this.stringData.loginError,
        subTitle: this.stringData.loginErrorSub,
        buttons: [this.stringData.confirmOK]
      });
      alert.present();
    }
  }

  checkIos() {
    if (this.platform.is('ios')) {
      this.isIos = true;
    }
    else {
      this.isIos = false;
    }
  }
}
