import { Component } from '@angular/core';
import { ViewController, AlertController, NavParams } from 'ionic-angular';
import { LangData } from "../../providers/language/lang.data";
import {Language} from "../../providers/language/lang";

@Component({
  selector: 'page-user-popover',
  template: `
    
    <ion-list style="vertical-align: middle; text-align: center;">
      <p>{{ showName() }}</p>
      <ion-item (click)="Logout()" >  
        <ion-icon item-start name="log-out"></ion-icon>
        <p end >{{ stringData.logout }}</p>
        
      </ion-item>
      
    </ion-list>
  `
})
export class UserPopoverPage {

  user: any;

  stringData: any;

  constructor(public viewCtrl: ViewController, private alertCtrl: AlertController,
              private nav: NavParams,
              public langData: LangData,
              public lang: Language) {
    this.user = this.nav.get('data');
    console.log(JSON.stringify(this.user));

    // this.globalization.getPreferredLanguage()
    //   .then(res => {
    //     this.lang.setLanguage(res.value);
    //     this.langData.init();
    //     this.stringData = this.langData.getLanguagePopOver();
    //   })
    //   .catch(e => console.log(e));
    this.stringData = this.langData.getLanguagePopOver();

  }

  Logout() {
    let alert = this.alertCtrl.create({
      title: this.stringData.confirmTitle,
      buttons: [
        {
          text: this.stringData.confirmCancel,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: this.stringData.confirmLogout,
          handler: () => {
            /**
             * delete localstorage for user
             */
            localStorage.removeItem('user-session');
            localStorage.removeItem('getResidencesFor' + this.user.id);

            /**
             * reload page back to login
             */
            location.reload();
          }
        }
      ]
    });
    alert.present();
  }

  showName() {
    var name = this.user.firstname + ' ' + this.user.lastname;
    if( name.length > 20) {
      return name.substring(0,20);
    }
    else {
      return name;
    }
  }

}
