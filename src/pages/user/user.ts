import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, LoadingController } from 'ionic-angular';
import {ModalPage} from '../modal/modal';
import {Device} from "@ionic-native/device";
import { LangData } from "../../providers/language/lang.data";
import {Language} from "../../providers/language/lang";
import {ENV} from "../../config/mqtt.environment";

declare let Paho: any;

@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  /**
   * for mqtt variables
   * @type {string}
   */
  client: any;
  requestGetAllResidenceDate: any;
  requestSetStatusHouseDate: any;
  requestUpdateNoticeDate: any;
  apiResponse = ENV().API_RESPONSE;
  apiRequest = ENV().API_REQUEST;
  allResidenceApiUrl = ENV().API_RES_DEV;
  setStatusHouseApiUrl = ENV().API_HOUSE_DEV;
  updateNoticeApiUrl = ENV().API_NOTIFICATION
  message: any;


  features = 'houses';
  user: any;
  residence: any;
  houses = [];
  messages = [];

  isToggleChange= false;

  loading: any;

  isRefresh = false;

  stringData: any;

  timeout: boolean;
  isCreateMes: boolean;
  isEditMes: boolean;
  isDeleteMes: boolean;
  isAlert = false;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private modalCtrl: ModalController,
              public loadingCtrl: LoadingController,
              private device: Device,
              public  langData: LangData,
              public lang: Language) {
    this.user = this.navParams.get('userData');
    this.residence = this.navParams.get('residenceData');
    // this.globalization.getPreferredLanguage()
    //   .then(res => {
    //     this.lang.setLanguage(res.value);
    //     this.langData.init();
    //     this.stringData = this.langData.getLanguageUser();
    //   })
    //   .catch(e => console.log(e));
    this.stringData = this.langData.getLanguageUser();


    this.presentLoadingDefalut();
    this.loading.present();
    this.getAllResidenceMqttConnect();
    if (!this.isAlert) {
      this.timeout = true;
      this.isAlert = true;
      var interval = setInterval(()=>{
        if (this.timeout) {
          clearInterval(interval);
          this.isAlert = false;
          this.loading.dismiss();
          let alert = this.alertCtrl.create({
            title: this.stringData.connectLost,
            buttons: [{
              text: this.stringData.confirmOK,
              handler: () => {

              }
            }]
          });
          alert.present();
        }
        else {
          this.isAlert = false;
          clearInterval(interval);
        }

      },60000);
    }
  }

  onInputHouse(input) {
    // Reset items back to all of the items
    this.initializeHouseMenu();

    // set q to the value of the searchbar
    var q = input.srcElement.value;


    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }

    this.houses = this.houses.filter((v) => {
      if(v['house_no'] && v.account.username && q ) {
        if (v['house_no'].toLowerCase().indexOf(q.toLowerCase()) > -1 || v.account.username.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  onInputMessage(input) {
    // Reset items back to all of the items
    this.initializeMessageMenu();

    // set q to the value of the searchbar
    var q = input.srcElement.value;


    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }

    this.messages = this.messages.filter((v) => {
      if(v.topic && v.message && q ) {
        if (v.topic.toLowerCase().indexOf(q.toLowerCase()) > -1 || v.message.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  initializeHouseMenu() {
    this.houses = this.residence.houses;
  }

  initializeMessageMenu () {
    var mes = this.residence.messages;
    this.messages = this.sortNotices(mes);
  }

  changeEnableHouse (toggle, index) {
    if (this.isToggleChange) {
      this.isToggleChange = false;
    }
    else {
      var checked = toggle.checked;
      let alert = this.alertCtrl.create({
        title: this.stringData.confirmChangeTitle,
        buttons: [
          {
            text: this.stringData.confirmCancel,
            role: 'cancel',
            handler: () => {
              // console.log('Cancel clicked');
              this.isToggleChange = true;
              toggle.checked = !checked;
            }
          },
          {
            text: this.stringData.confirmOK,
            handler: () => {
              this.presentLoadingDefalut();
              this.loading.present();
              this.houses[index].isEnable = toggle.checked;
              this.setStatusHouseMqttConnect(this.houses[index], toggle);
              if (!this.isAlert) {
                this.timeout = true;
                this.isAlert = true;
                var interval = setInterval(()=>{
                  if (this.timeout) {
                    clearInterval(interval);
                    this.isAlert = false;
                    this.isToggleChange = true;
                    this.loading.dismiss();
                    toggle.checked = !checked;
                    this.houses[index].isEnable = toggle.checked;
                    let alert = this.alertCtrl.create({
                      title: this.stringData.connectLost,
                      buttons: [{
                        text: this.stringData.confirmOK,
                        handler: () => {

                        }
                      }]
                    });
                    alert.present();
                  }
                  else {
                    this.isAlert = false;
                    clearInterval(interval);
                  }

                },60000);
              }
            }
          }
        ]
      });
      alert.present();
    }
  }

  openCreateMessage () {
    let profileModal = this.modalCtrl.create(ModalPage, { type: 'createMessage',
      data: this.user}, {showBackdrop: false});
    profileModal.onDidDismiss(data => {
      if (data != null ) {
        if (data.type != 'dismiss') {
          this.presentLoadingDefalut();
          this.loading.present();
          this.isCreateMes = true;
          this.updateNoticeMqttConnect('create', data);
          if (!this.isAlert) {
            this.timeout = true;
            this.isAlert = true;
            var interval = setInterval(()=>{
              if (this.timeout) {
                clearInterval(interval);
                this.isAlert = false;
                this.loading.dismiss();
                let alert = this.alertCtrl.create({
                  title: this.stringData.connectLost,
                  buttons: [{
                    text: this.stringData.confirmOK,
                    handler: () => {

                    }
                  }]
                });
                alert.present();
              }
              else {
                this.isAlert = false;
                clearInterval(interval);
              }

            },60000);
          }
        }
      }

    });
    profileModal.present();
  }

  showDescription (d) {
    if (d.length > 100) {
      return d.substring(0, 40) + '...';
    }
    else {
      return d;
    }
  }

  openNotice(m) {
    let profileModal = this.modalCtrl.create(ModalPage, { type: 'openNotice',
      data: this.user, notice: m}, {showBackdrop: false});
    profileModal.onDidDismiss(data => {
    });
    profileModal.present();
  }

  editNotice(m) {
    let profileModal = this.modalCtrl.create(ModalPage, { type: 'editNotice',
      data: this.user, notice: m}, {showBackdrop: false});
    profileModal.onDidDismiss(data => {
      if (data != null ) {
        if (data.type != 'dismiss') {
          this.presentLoadingDefalut();
          this.loading.present();
          this.isEditMes = true;
          this.updateNoticeMqttConnect('update', data);
          if (!this.isAlert) {
            this.timeout = true;
            this.isAlert = true;
            var interval = setInterval(()=>{
              if (this.timeout) {
                clearInterval(interval);
                this.isAlert = false;
                this.loading.dismiss();
                let alert = this.alertCtrl.create({
                  title: this.stringData.connectLost,
                  buttons: [{
                    text: this.stringData.confirmOK,
                    handler: () => {

                    }
                  }]
                });
                alert.present();
              }
              else {
                this.isAlert = false;
                clearInterval(interval);
              }

            },60000);
          }
        }
      }
    });
    profileModal.present();
  }

  deleteNotice(m) {
    let alert = this.alertCtrl.create({
      title: this.stringData.confirmDeleteTitle,
      buttons: [
        {
          text: this.stringData.confirmCancel,
          role: 'cancel',
          handler: () => {
            // console.log('Cancel clicked');
          }
        },
        {
          text: this.stringData.confirmOK,
          handler: () => {
            this.presentLoadingDefalut();
            this.loading.present();
            this.isDeleteMes = true;
            this.updateNoticeMqttConnect('delete', {
              id: m['_id'],
              residenceName: this.residence.name
            });
            if (!this.isAlert) {
              this.timeout = true;
              this.isAlert = true;
              var interval = setInterval(()=>{
                if (this.timeout) {
                  clearInterval(interval);
                  this.isAlert = false;
                  this.loading.dismiss();
                  let alert = this.alertCtrl.create({
                    title: this.stringData.connectLost,
                    buttons: [{
                      text: this.stringData.confirmOK,
                      handler: () => {

                      }
                    }]
                  });
                  alert.present();
                }
                else {
                  this.isAlert = false;
                  clearInterval(interval);
                }

              },2000);
            }
          }
        }
      ]
    });
    alert.present();
  }

  findResidenceData(l) {
    for (var res of l) {
      if (this.residence.name === res.name) {
        this.residence.houses = res.houses;
        this.residence.messages = res.notifications;
        return true;
      }
    }
    return false;
  }

  presentLoadingDefalut() {
    this.loading = this.loadingCtrl.create({
      content: this.stringData.pleaseWait
    });
  }

  millisecondsToDate(d, type) {
    var date = new Date(d);
    var year = date.getFullYear();
    var month = date.getMonth() +1;
    var day = date.getDate();
    var time = date.getTime();
    var hours = date.getHours();
    var minutes = parseInt(((time / (1000 * 60 )) % 60) + '');
    var hour = (hours < 10) ? "0" + hours : hours;
    var minute = (minutes < 10) ? "0" + minutes : minutes;

    if (type == 'withTime') {
      return day + '/' + month + '/' + year + ' ' + hour + ':' + minute;
    }
    else if (type == 'withoutTime') {
      return day + '/' + month + '/' + year;
    }
  }

  doRefresh(refresher) {
    this.isRefresh = true;
    this.getAllResidenceMqttConnect();
    setTimeout(() => {
      refresher.complete();
      this.isRefresh = false;
    }, 2000);
  }

  sortNotices(list) {
    list.sort(function(a, b) {
      return parseFloat(a.sendDate) - parseFloat(b.sendDate);
    });
    list.reverse();
    return list
  }

  /**
   * function that below this , is mqtt function connection
   */
  getAllResidenceMqttConnect() {
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      this.timeout = false;
      var serveres = JSON.parse(mesdata.payloadString);
      if (serveres.header.requestId == this.requestGetAllResidenceDate.toString()) {
        if (serveres.value.hasOwnProperty('error')) {
          /**
           * failed to get all user developer
           */
          if (!this.isRefresh) {
            this.loading.dismiss();
          }

          alert(serveres.value.error);
        }
        else {
          /**
           * get all user developer
           */

          if (this.findResidenceData(serveres.value)) {
            this.initializeHouseMenu();
            this.initializeMessageMenu();

          }
          if (!this.isRefresh) {
            this.loading.dismiss();
            if (this.isCreateMes) {
              let alert = this.alertCtrl.create({
                title: this.stringData.confirmCreateMes,
                buttons: [{
                  text: this.stringData.confirmOK,
                  handler: () => {
                    this.isCreateMes = false;
                  }
                }]
              });
              alert.present();
            }
            if (this.isEditMes) {
              let alert = this.alertCtrl.create({
                title: this.stringData.confirmEditMes,
                buttons: [{
                  text: this.stringData.confirmOK,
                  handler: () => {
                    this.isEditMes = false;
                  }
                }]
              });
              alert.present();
            }
            if (this.isDeleteMes) {
              let alert = this.alertCtrl.create({
                title: this.stringData.confirmDeleteMes,
                buttons: [{
                  text: this.stringData.confirmOK,
                  handler: () => {
                    this.isDeleteMes = false;
                  }
                }]
              });
              alert.present();
            }
          }
          // console.log('User Value : ' + JSON.stringify(serveres.value).length);

        }
        this.client.unsubscribe(this.allResidenceApiUrl + this.user.id + this.apiResponse);
      }

    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        this.requestGetAllResidenceDate = new Date().getTime();
        this.requestGetAllResidenceDate = parseInt(this.requestGetAllResidenceDate);
        this.client.subscribe(this.allResidenceApiUrl + this.user.id + this.apiResponse, {qos: 2});
        var m = JSON.stringify({
          header: {
            requestId: this.requestGetAllResidenceDate + "",
            command: 'get',
            timestamp: this.requestGetAllResidenceDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: {
            developerName: this.user.developer,
          }
        });
        // console.log(this.user.developer + ' : developer Name');
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.allResidenceApiUrl + this.user.id + this.apiRequest;
        this.client.send(this.message);
      },
      onFailure: () => {
        if(this.loading != null) {
          this.loading.dismiss();
          this.timeout = false;
          let alert = this.alertCtrl.create({
            title: this.stringData.connectLost,
            buttons: [{
              text: this.stringData.confirmOK,
              handler: () => {
              }
            }]
          });
          alert.present();
        }


      }
    });
  }

  setStatusHouseMqttConnect(house, toggle) {
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      this.timeout = false;
      var serveres = JSON.parse(mesdata.payloadString);
      if (serveres.header.requestId == this.requestSetStatusHouseDate.toString()) {
        if (serveres.value.hasOwnProperty('error')) {
          /**
           * failed to set status of house
           */
          this.loading.dismiss();
          alert(serveres.value.error);
        }
        else {
          /**
           * success to set status of house
           */

          this.loading.dismiss();

          // console.log('house status : ' + JSON.stringify(serveres.value));

        }
        this.client.unsubscribe(this.setStatusHouseApiUrl + this.residence.id + this.apiResponse);
      }

    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        this.requestSetStatusHouseDate = new Date().getTime();
        this.requestSetStatusHouseDate = parseInt(this.requestSetStatusHouseDate);
        this.client.subscribe(this.setStatusHouseApiUrl + this.residence.id + this.apiResponse, {qos: 2});
        var m = JSON.stringify({
          header: {
            requestId: this.requestSetStatusHouseDate + "",
            command: 'update',
            timestamp: this.requestSetStatusHouseDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: {
            residenceName: this.residence.name,
            houseNo: house['house_no'],
            isEnable: house.isEnable

          }
        });
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.setStatusHouseApiUrl + this.residence.id + this.apiRequest;
        this.client.send(this.message);
      },
      onFailure: () => {
        this.timeout = false;
        this.isToggleChange = true;
        toggle.checked = !toggle.checked;
        this.houses[house] = toggle.checked;
        let alert = this.alertCtrl.create({
          title: this.stringData.connectLost,
          buttons: [{
            text: this.stringData.confirmOK,
            handler: () => {
              this.loading.dismiss();
            }
          }]
        });
        alert.present();
      }
    });
  }

  updateNoticeMqttConnect(type, data) {
    this.client = new Paho.MQTT.Client(
      ENV().API_HOST,
      ENV().API_PORT,
      ENV().API_WS,
      this.device.uuid
    );

    this.client.onConnectionLost = ((responseObject) => {
      if (responseObject.errorCode !== 0) {
        console.log("onConnectionLost:" + responseObject.errorMessage);
      }
    });

    this.client.onMessageArrived = ((mesdata) => {
      this.timeout = false;
      var serveres = JSON.parse(mesdata.payloadString);
      if (serveres.header.requestId == this.requestUpdateNoticeDate.toString()) {
        if (serveres.value.hasOwnProperty('error')) {
          /**
           * failed to update notice
           */
          this.loading.dismiss();
          alert(serveres.value.error);
        }
        else {
          /**
           * success to update notice
           */


          // console.log('update notice data : ' + JSON.stringify(serveres.value));
          this.getAllResidenceMqttConnect();
        }
        this.client.unsubscribe(this.updateNoticeApiUrl + this.residence.id + this.apiResponse);
      }

    });

    // connect the client
    this.client.connect({
      userName: ENV().API_USERNAME,
      password: ENV().API_PASSWORD,
      useSSL: ENV().API_SSL,
      timeout: ENV().API_TIMEOUT,
      onSuccess: () => {
        this.requestUpdateNoticeDate = new Date().getTime();
        this.requestUpdateNoticeDate = parseInt(this.requestUpdateNoticeDate);
        this.client.subscribe(this.updateNoticeApiUrl + this.residence.id + this.apiResponse, {qos: 2});
        var value = {};
        switch (type) {
          case 'create':
            value = {
              topics: data.topic,
              message: data.message,
              sendDate: new Date(data.sentDate).getTime(),
              residenceName: this.residence.name
            };
            break;
          case 'update':
            value = {
              notificationId: data.id,
              topic: data.topic,
              message: data.message,
              sendDate: new Date(data.sentDate).getTime(),
              residenceName: this.residence.name
            };
            break;
          case 'delete':
            value = {
              notificationId: data.id,
              residenceName: this.residence.name
            };
            break;

        }
        // console.log('value : ' + JSON.stringify(value));
        var m = JSON.stringify({
          header: {
            requestId: this.requestUpdateNoticeDate + "",
            command: type,
            timestamp: this.requestUpdateNoticeDate + "",
            clientId: this.device.uuid
            // clientId: 12345967412368745364769345
          },
          value: value
        });
        this.message = new Paho.MQTT.Message(m);
        this.message.destinationName = this.updateNoticeApiUrl + this.residence.id + this.apiRequest;
        this.client.send(this.message);
      },
      onFailure: () => {
        this.timeout = false;
        let alert = this.alertCtrl.create({
          title: this.stringData.connectLost,
          buttons: [{
            text: this.stringData.confirmOK,
            handler: () => {
              this.loading.dismiss();
            }
          }]
        });
        alert.present();
      }
    });
  }
}
