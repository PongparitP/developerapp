import {Injectable} from '@angular/core';
import { Language } from "../language/lang";
import {Globalization} from "@ionic-native/globalization";

@Injectable()
export class LangData {
  /**
   * language type name exp. 'en-EN' or 'th-TH'
   */
  langName: string;

  /**
   * array of string that send to another page
   */
  appLang: any;
  homeLang: any;
  popLang: any;
  adminLang: any;
  userLang: any;
  modalLang: any;

  constructor(public lang: Language, private globalization: Globalization) {
    var interval = setInterval(()=>{
     var ready = localStorage.getItem('platformReady');
     if (ready) {
       this.globalization.getPreferredLanguage()
         .then(res => {
           this.lang.setLanguage(res.value);
           this.init();
           clearInterval(interval);
           console.log(this.langName);
         })
         .catch(e => console.log(e));
     }
    });

  }

  init() {
    this.langName = this.lang.getLanguage();
    // console.log(this.langName);
      this.setLanguageApp();
      this.setLanguageHome();
      this.setLanguagePopOver();
      this.setLanguageAdmin();
      this.setLanguageUser();
      this.setLanguageModal();


  }


  setLanguageApp() {
    if (this.langName === 'th-TH') {
      this.appLang = {
        updateAt: 'แก้ไขเมื่อ',
        userTitleLand: 'การจัดการหมู่บ้าน',
        adminTitleLand: 'การจัดการผู้ใช้',
        userTitlePort: 'หมู่บ้าน',
        adminTitlePort: 'ผู้ใช้',
        pleaseWait: 'รอสักครู่...',
        searchPlaceHolder: 'ค้นหา',
        toastMes: 'กดปุ่มย้อนกลับอีกครั้งเพื่อออกจากแอปพลิเคชัน',
        connectLost: 'ไม่สามารถเชื่อมต่อกับเซิร์ฟเวอร์ได้ กรุณาลองใหม่อีกครั้งภายหลัง',
        confirmOK: 'ตกลง',
        confirmAddUser: 'เพิ่มผู้ใช้สำเร็จ',
        errLogin: 'เกิดข้อผิดพลาดระหว่างเข้าสู่ระบบ'
      };
    }
    else {
      this.appLang = {
        updateAt: 'Updated at',
        userTitleLand: 'Residence Management',
        adminTitleLand: 'User Management',
        userTitlePort: 'Residence',
        adminTitlePort: 'User',
        pleaseWait: 'Please wait...',
        searchPlaceHolder: 'Search',
        toastMes: 'Press back again to exit app.',
        connectLost: 'Cannot connect to server, Please try again later.',
        confirmOK: 'OK',
        confirmAddUser: 'Add user complete',
        errLogin: 'Error while logging in.'
      };
    }
  }

  setLanguageHome () {
    if (this.langName === 'th-TH') {
      this.homeLang = {
        userSelect: 'เลือกหมู่บ้านเพื่อจัดการ',
        adminSelect: 'เลือกผู้ใช้เพื่อจัดการ'
      }
    }
    else {
      this.homeLang = {
        userSelect: 'Select residence to manage.',
        adminSelect: 'Select a user to residence permission.'
      }
    }
  }

  setLanguagePopOver() {
    if (this.langName === 'th-TH') {
      this.popLang = {
        logout: 'ออกจากระบบ',
        confirmTitle: 'คุณแน่ใจแล้วหรือไม่ที่จะออกจากระบบ?',
        confirmCancel: 'ยกเลิก',
        confirmLogout: 'ออกจากระบบ'
      }
    }
    else {
      this.popLang = {
        logout: 'Logout',
        confirmTitle: 'Are you sure you want to logout?',
        confirmCancel: 'CANCEL',
        confirmLogout: 'LOGOUT'
      }
    }
  }

  setLanguageAdmin() {
    if (this.langName === 'th-TH') {
      this.adminLang = {
        email: 'อีเมล',
        phone: 'เบอร์โทรศัพท์',
        editProfile: 'แก้ไขข้อมูลผู้ใช้',
        confirmChangeTitle: 'คุณแน่ใจแล้วหรือไม่ที่จะเปลี่ยนแปลง?',
        confirmCancel: 'ยกเลิก',
        confirmOK: 'ตกลง',
        pleaseWait: 'รอสักครู่...',
        connectLost: 'ไม่สามารถเชื่อมต่อกับเซิร์ฟเวอร์ได้ กรุณาลองใหม่อีกครั้งภายหลัง',
        confirmEditUser: 'แก้ไขข้อมูลสำเร็จ'
      }
    }
    else {
      this.adminLang = {
        email: 'Email',
        phone: 'Phone',
        editProfile: 'EDIT PROFILE',
        confirmChangeTitle: 'Are you sure you want to change ?',
        confirmCancel: 'CANCEL',
        confirmOK: 'OK',
        pleaseWait: 'Please wait...',
        connectLost: 'Cannot connect to server, Please try again later.',
        confirmEditUser: 'Edit profile success.'
      }
    }
  }

  setLanguageUser() {
    if (this.langName === 'th-TH') {
      this.userLang = {
        house: 'บ้าน',
        message: 'ข้อความ',
        houseNo: 'บ้านเลขที่',
        username: 'ชื่อผู้ใช้',
        createMessage: 'สร้างข้อความ',
        modify: 'แก้ไข',
        delete: 'ลบทิ้ง',
        hasSent: 'ส่งแล้ว',
        confirmDeleteTitle: 'คุณแน่ใจแล้วหรือไม่ที่จะลบ?',
        confirmChangeTitle: 'คุณแน่ใจแล้วหรือไม่ที่จะเปลี่ยนแปลง?',
        confirmCancel: 'ยกเลิก',
        confirmOK: 'ตกลง',
        pleaseWait: 'รอสักครู่...',
        searchPlaceHolder: 'ค้นหา',
        connectLost: 'ไม่สามารถเชื่อมต่อกับเซิร์ฟเวอร์ได้ กรุณาลองใหม่อีกครั้งภายหลัง',
        confirmCreateMes: 'สร้างข้อความสำเร็จ',
        confirmEditMes: 'แก้ไขข้อความสำเร็จ',
        confirmDeleteMes: 'ลบข้อความสำเร็จ'
      }
    }
    else {
      this.userLang = {
        house: 'House',
        message: 'Message',
        houseNo: 'House No.',
        username: 'Username',
        createMessage: 'CREATE MESSAGE',
        modify: 'Modify',
        delete: 'Delete',
        hasSent: 'has sent',
        confirmChangeTitle: 'Are you sure you want to change ?',
        confirmDeleteTitle: 'Are you sure you want to delete ?',
        confirmCancel: 'CANCEL',
        confirmOK: 'OK',
        pleaseWait: 'Please wait...',
        searchPlaceHolder: 'Search',
        connectLost: 'Cannot connect to server, Please try again later.',
        confirmCreateMes: 'Message has been created.',
        confirmEditMes: 'Message has been edited.',
        confirmDeleteMes: 'Message has been deleted.'
      }
    }
  }

  setLanguageModal() {
    if (this.langName == 'th-TH') {
      this.modalLang = {
        username: 'ชื่อผู้ใช้',
        password: 'รหัสผ่าน',
        login: 'เข้าสู่ระบบ',
        firstname: 'ชื่อ',
        lastname: 'นามสกุล',
        cfmPassword: 'ยืนยันรหัสผ่าน',
        title: 'หัวข้อ',
        description: 'คำอธิบาย',
        date: 'วันเวลา',
        selectDate: 'เลือกวันเวลา',
        create: 'สร้าง',
        information: 'ข้อมูล',
        editMessage: 'แก้ไขข้อความ',
        editProfile: 'แก้ไขผู้ใช้',
        addUser: 'เพิ่มผู้ใช้',
        createMessage: 'สร้างข้อความ',
        email: 'อีเมล',
        phone: 'เบอร์โทรศัพท์',
        confirmCancel: 'ยกเลิก',
        confirmOK: 'ตกลง',
        confirmSave: 'บันทึก',
        passwordErrorTitle: 'รหัสผ่านผิดพลาด',
        passwordErrorSubTitle: 'รหัสผ่านและยืนยันรหัสผ่านไม่ตรงกัน กรุณาลองอีกครั้ง',
        emailErrorTitle: 'รูปแบบอีเมลไม่ถูกต้อง',
        emailErrorSubTitle: 'กรุณาใส่อีเมลอีกครั้ง',
        addUserErrorTitle: 'เพิ่มผู้ใช้ผิดพลาด',
        addUserErrorSubTitle: 'กรุณากรอกข้อมูลให้ครบ',
        loginError: 'เข้าสู่ระบบผิดพลาด',
        loginErrorSub: 'กรุณากรอกข้อมูลให้ครบ',
        addUserFirstNameErrorTitle: 'เพิ่มผู้ใช้ผิดพลาด',
        addUserFirstNameErrorSubTitle: 'กรุณากรอกชื่อก่อนเพิ่มผู้ใช้',
        addUserLastNameErrorTitle: 'เพิ่มผู้ใช้ผิดพลาด',
        addUserLastNameErrorSubTitle: 'กรุณากรอกนามสกุลก่อนเพิ่มผู้ใช้',
        addUserUsernameErrorTitle: 'เพิ่มผู้ใช้ผิดพลาด',
        addUserUsernameErrorSubTitle: 'กรุณากรอกชื่อผู้ใช้ก่อนเพิ่มผู้ใช้',
        addUserPasswordErrorTitle: 'เพิ่มผู้ใช้ผิดพลาด',
        addUserPasswordErrorSubTitle: 'กรุณากรอกรหัสผ่านก่อนเพิ่มผู้ใช้',
        addUserCfmPasswordErrorTitle: 'เพิ่มผู้ใช้ผิดพลาด',
        addUserCfmPasswordErrorSubTitle: 'กรุณากรอกยืนยันรหัสผ่านก่อนเพิ่มผู้ใช้',
        addUserEmailErrorTitle: 'เพิ่มผู้ใช้ผิดพลาด',
        addUserEmailErrorSubTitle: 'กรุณากรอกอีเมลก่อนเพิ่มผู้ใช้',
        addUserPhoneErrorTitle: 'เพิ่มผู้ใช้ผิดพลาด',
        addUserPhoneErrorSubTitle: 'กรุณากรอกเบอร์โทรศัพท์ก่อนเพิ่มผู้ใช้',
        editUserFirstNameErrorTitle: 'แก้ไขผู้ใช้ผิดพลาด',
        editUserFirstNameErrorSubTitle: 'กรุณากรอกชื่อก่อนแก้ไขผู้ใช้',
        editUserLastNameErrorTitle: 'แก้ไขผู้ใช้ผิดพลาด',
        editUserLastNameErrorSubTitle: 'กรุณากรอกนามสกุลก่อนแก้ไขผู้ใช้',
        editUserUsernameErrorTitle: 'แก้ไขผู้ใช้ผิดพลาด',
        editUserUsernameErrorSubTitle: 'กรุณากรอกชื่อผู้ใช้ก่อนแก้ไขผู้ใช้',
        editUserPasswordErrorTitle: 'แก้ไขผู้ใช้ผิดพลาด',
        editUserPasswordErrorSubTitle: 'กรุณากรอกรหัสผ่านก่อนแก้ไขผู้ใช้',
        editUserCfmPasswordErrorTitle: 'แก้ไขผู้ใช้ผิดพลาด',
        editUserCfmPasswordErrorSubTitle: 'กรุณากรอกยืนยันรหัสผ่านก่อนแก้ไขผู้ใช้',
        editUserEmailErrorTitle: 'แก้ไขผู้ใช้ผิดพลาด',
        editUserEmailErrorSubTitle: 'กรุณากรอกอีเมลก่อนแก้ไขผู้ใช้',
        editUserPhoneErrorTitle: 'แก้ไขผู้ใช้ผิดพลาด',
        editUserPhoneErrorSubTitle: 'กรุณากรอกเบอร์โทรศัพท์ก่อนแก้ไขผู้ใช้',
        createMesErrorTitle: 'สร้างข้อความผิดพลาด',
        createMesErrorSubTitle: 'กรุณากรอกช่องหัวข้อ คำอธิบายและเลือกเวลาให้ครบ',
        updateMesErrorTitle: 'แก้ไขข้อความผิดพลาด',
        updateMesErrorSubTitle: 'กรุณากรอกช่องหัวข้อ คำอธิบายและเลือกเวลาให้ครบ',
        timeMesErrorTitle: 'เวลาผิดพลาด',
        timeMesErrorSubTitle: 'กรุณากรอกวัน เวลาหลังปัจจุบัน'
      }
    }
    else {
      this.modalLang = {
        username: 'Username',
        password: 'Password',
        login: 'LOGIN',
        firstname: 'Firstname',
        lastname: 'Lastname',
        cfmPassword: 'Confirm Password',
        title: 'Title',
        description: 'Description',
        date: 'Date',
        selectDate: 'Select Date',
        create: 'CREATE',
        information: 'Information',
        editMessage: 'Edit Message',
        editProfile: 'Edit Profile',
        addUser: 'Add User',
        createMessage: 'Create Message',
        email: 'Email',
        phone: 'Phone',
        confirmCancel: 'CANCEL',
        confirmOK: 'OK',
        confirmSave: 'SAVE',
        passwordErrorTitle: 'Password error',
        passwordErrorSubTitle: 'Password and confirm password is not match, Please try again.',
        emailErrorTitle: 'Email format error',
        emailErrorSubTitle: 'Please enter email again.',
        addUserErrorTitle: 'Add user error',
        addUserErrorSubTitle: 'Please fill all input',
        loginError: 'Login error',
        loginErrorSub: 'Please fill all input',
        addUserFirstNameErrorTitle: 'Add user error',
        addUserFirstNameErrorSubTitle: 'Please fill firstname.',
        addUserLastNameErrorTitle: 'Add user error',
        addUserLastNameErrorSubTitle: 'Please fill lastname.',
        addUserUsernameErrorTitle: 'Add user error',
        addUserUsernameErrorSubTitle: 'Please fill username.',
        addUserPasswordErrorTitle: 'Add user error',
        addUserPasswordErrorSubTitle: 'Please fill password.',
        addUserCfmPasswordErrorTitle: 'Add user error',
        addUserCfmPasswordErrorSubTitle: 'Please fill confirm password.',
        addUserEmailErrorTitle: 'Add user error',
        addUserEmailErrorSubTitle: 'Please fill email.',
        addUserPhoneErrorTitle: 'Add user error',
        addUserPhoneErrorSubTitle: 'Please fill phone.',
        editUserFirstNameErrorTitle: 'Edit user error',
        editUserFirstNameErrorSubTitle: 'Please fill firstname.',
        editUserLastNameErrorTitle: 'Edit user error',
        editUserLastNameErrorSubTitle: 'Please fill lastname.',
        editUserUsernameErrorTitle: 'Edit user error',
        editUserUsernameErrorSubTitle: 'Please fill username.',
        editUserPasswordErrorTitle: 'Edit user error',
        editUserPasswordErrorSubTitle: 'Please fill password.',
        editUserCfmPasswordErrorTitle: 'Edit user error',
        editUserCfmPasswordErrorSubTitle: 'Please fill confirm password.',
        editUserEmailErrorTitle: 'Edit user error',
        editUserEmailErrorSubTitle: 'Please fill email.',
        editUserPhoneErrorTitle: 'Edit user error',
        editUserPhoneErrorSubTitle: 'Please fill phone.',
        createMesErrorTitle: 'Create message error',
        createMesErrorSubTitle: 'Please fill title, description and date input.',
        updateMesErrorTitle: 'Update message error',
        updateMesErrorSubTitle: 'Please fill title, description and date input.',
        timeMesErrorTitle: 'sent time error',
        timeMesErrorSubTitle: 'time cannot less than now'
      }
    }
  }

  getLanguageModal() {
    return this.modalLang;
  }

  getLanguageUser() {
    return this.userLang;
  }

  getLanguageAdmin() {
    return this.adminLang;
  }

  getLanguagePopOver() {
    return this.popLang;
  }

  getLanguageHome() {
    return this.homeLang;
  }

  getLanguageApp() {
    return this.appLang;
  }


}
