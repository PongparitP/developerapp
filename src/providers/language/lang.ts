import {Injectable} from '@angular/core';

@Injectable()
export class Language {

  name: string;

  constructor() {
    this.name = '';
  }

  setLanguage(value) {
    this.name = value;
  }

  getLanguage() {
    return this.name;
  }

}
