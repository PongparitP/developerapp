import {Injectable} from '@angular/core';

@Injectable()
export class Users {

  users: any;

  constructor() {
    this.users = [];
  }

  setUsers(value) {
    this.users = value;
  }

  getUsers() {
    return this.users;
  }

}
